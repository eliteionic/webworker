import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	items: any[] = new Array(50);

	constructor(public navCtrl: NavController, public dataProvider: DataProvider) {

	}

	ionViewDidLoad(){
		
	}

	startSort(){
		this.dataProvider.doSortWithWorker();
	}

}
