import { Injectable } from '@angular/core';

@Injectable()
export class DataProvider {

	private sortWorker: Worker;

	constructor() {

	}

	doSort(){

		let randomArray = [];

		console.log("create array");

		for(let i=0; i<1000000; i++){
			randomArray.push(Math.random());
		}

		console.log("start sort");

		randomArray.sort();

		console.log("sort finished");

	}

	doSortWithWorker(){

		this.sortWorker = new Worker('../assets/js/doSort.js');

		this.sortWorker.addEventListener('message', (e) => {
			console.log("done!");
			//console.log(e.data);
		}, false);

		this.sortWorker.postMessage(1000000);

	}

}
